import React, { createContext, useState, useEffect } from "react";
import { i18n } from "../i18n";

export const Context = createContext();
const MainContext = ({ children }) => {
  useEffect(() => {
    const getCategoriesAndProducts = async () => {
      try {
        await getCategories();
        await getProducts();
        setIsLoading(false);
      } catch (e) {
        return e;
      }
    };
    if (isLoading) getCategoriesAndProducts();
  }, []);
  const [categories, setCategories] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [banners, setBanners] = useState([]);
  const [currentProducts, setCurrentProducts] = useState([]);
  const [filteredProduct, setFilteredProduct] = useState([]);
  const [products, setProducts] = useState([]);
  const getCategories = async () => {
    try {
      let data = await fetch(`${process.env.STRAPI_URL}/kategoris`);
      let json = await data.json();
      setCategories(
        json.sort((a, b) => {
          return Math.abs(a.id - 2) - Math.abs(b.id - 2);
        })
      );
    } catch (e) {
      return e;
    }
  };
  const getProducts = async () => {
    try {
      let data = await fetch(`${process.env.STRAPI_URL}/urunlers`);
      let banners = await fetch(`${process.env.STRAPI_URL}/banners`);
      let bannersJson = await banners.json();
      setBanners(bannersJson);
      let json = await data.json();
      setProducts(json.filter((item) => item.image));
      setCurrentProducts(json.filter((item) => item.image));
    } catch (e) {
      return e;
    }
  };
  const getProductByCategory = (categoryId) => {
    const products = categories.find((item) => item.id === categoryId);
    setCurrentProducts(products.urunlers.filter((item) => item.image));
  };
  const getProductImagesByCategoryId = (categoryId, prod) => {
    const products = categories.find((item) => item.id == categoryId);
    setFilteredProduct(
      products.urunlers
        .filter((i) => i.image && i[`name_${i18n.language}`] === prod)
        .reverse()
    );
    return products.urunlers;
  };

  const getProductsByName = async (query) => {
    try {
      let data = await fetch(`${process.env.STRAPI_URL}/urunlers?${query}`);
      let json = await data.json();
      return json.filter((i) => i.image);
    } catch (e) {
      return e;
    }
  };
  const getBlogImages = async () => {
    try {
      let data = await fetch(`${process.env.STRAPI_URL}/galeris`);
      let json = await data.json();
      return json;
    } catch (e) {
      return e;
    }
  };

  return (
    <Context.Provider
      value={{
        categories,
        currentProducts,
        getProductByCategory,
        products,
        banners,
        getProducts,
        getProductImagesByCategoryId,
        isLoading,
        filteredProduct,
        getProductsByName,
        getBlogImages,
      }}
    >
      {children}
    </Context.Provider>
  );
};
export default MainContext;
