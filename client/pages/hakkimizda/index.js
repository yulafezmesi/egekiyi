import React, { useEffect, useState } from "react";
import Head from "next/head";

import PropTypes from "prop-types";
import ReactMarkdown from "react-markdown";

import Layout from "../components/Layout/";
import { withTranslation, i18n } from "../../i18n";

const Home = ({ t }) => {
  const [about, setAbout] = useState({
    description_tr: "",
    description_en: "",
    policy_tr: "",
    policy_en: "",
  });
  useEffect(() => {
    const getAboutUs = async () => {
      try {
        let data = await fetch(`${process.env.STRAPI_URL}/hakkimizda`);
        let json = await data.json();
        setAbout(json);
        return json;
      } catch (e) {
        return e;
      }
    };
    getAboutUs();
  }, []);
  return (
    <>
      <Head>
        <title>Ege Kıyı | Hakkımızda</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Layout isPrimary={true}>
        <section className="content-wrapper">
          <div className="container">
            <div className="columns">
              <div
                style={{
                  justifyContent: "center",
                  display: "flex",
                  flexDirection: "column",
                }}
                className="column"
              >
                <figure className="image is-3by2">
                  <img
                    style={{ objectFit: "contain" }}
                    src="assets/img/hero/food.png"
                  />
                </figure>
              </div>

              <div className="column ">
                <div className="content is-medium">
                  <h2 className="title has-text-centered">{t("about")}</h2>
                  <ReactMarkdown
                    source={about[`description_${i18n.language}`]}
                  ></ReactMarkdown>
                </div>
              </div>
            </div>
            <div className="columns">
              <div className="column">
                <div className="content is-medium">
                  <h2 className="title has-text-centered">{t("policy")}</h2>
                  <ReactMarkdown
                    source={about[`policy_${i18n.language}`]}
                  ></ReactMarkdown>
                </div>
              </div>
              <div className="column">
                <div
                  style={{
                    justifyContent: "center",
                    display: "flex",
                    flexDirection: "column",
                  }}
                  className="column"
                >
                  <figure className="image is-5by3">
                    <img
                      style={{ objectFit: "contain" }}
                      src="assets/img/organic.png"
                    />
                  </figure>
                </div>
              </div>
            </div>
          </div>
        </section>
      </Layout>
    </>
  );
};
Home.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

Home.propTypes = {
  t: PropTypes.func.isRequired,
};

export default withTranslation("common")(Home);
