import App from "next/app";
import { appWithTranslation } from "../i18n";
import "react-image-lightbox/style.css"; // This only needs to be imported once in your app

import "../styles/globals.scss";
import "bulma/bulma.sass";

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

MyApp.getInitialProps = async (appContext) => ({
  ...(await App.getInitialProps(appContext)),
});

export default appWithTranslation(MyApp);
