import React, { useContext, useState, useEffect } from "react";
import { i18n, withTranslation } from "../../../i18n";
import Head from "next/head";
import Link from "next/link";
import { Context } from "../../context";
import Slider from "react-slick";
import Error from "../../_error";
import PropTypes from "prop-types";

const ProductDetail = ({ id, t }) => {
  const {
    products,
    getProductImagesByCategoryId,
    isLoading,
    categories,
  } = useContext(Context);

  if (isLoading) return null;
  const [product, setProduct] = useState({
    id: null,
    name_tr: "",
    name_en: "",
    gross: "",
    net: "",
    quantity: 0,
    type_tr: null,
    type_en: null,
    description_en: null,
    description_tr: null,
    image: {},
  });
  const [isCategoryLoading, setIsCategoryLoading] = useState(true);
  const [filteredProduct, setFilteredProduct] = useState([]);
  const [shuffledProduct, setShuffledProduct] = useState([]);

  useEffect(() => {
    const prod = products.find((i) => i.id == id);
    setProduct(prod);
    setFilteredProduct(
      getProductImagesByCategoryId(
        prod.kategori.id,
        prod[`name_${i18n.language}`]
      ).filter(
        (i) =>
          i.image &&
          i[`name_${i18n.language}`] === prod[`name_${i18n.language}`]
      )
    );
    setShuffledProduct(
      categories
        .find((item) => item.id == prod.kategori.id)
        .urunlers.filter((i) => i.image && i.id !== prod.id)
        .splice(Math.floor(Math.random() * categories.length), 2)
    );
    setIsCategoryLoading(false);
    if (!prod) return <Error />;
  }, [id]);

  const settings = {
    customPaging: function (i) {
      return (
        <a>
          {i <= filteredProduct.length ? <ProductThumbnail index={i} /> : null}
        </a>
      );
    },
    dots: true,
    dotsClass: "slick-dots slick-thumb",
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    beforeChange: (current, next) => {
      setProduct(filteredProduct[next]);
    },
  };

  const ProductThumbnail = ({ index }) => {
    let img = filteredProduct[index];
    if (!img) return null;
    return (
      <img
        style={{ maxHeight: "100px" }}
        src={process.env.STRAPI_URL + img.image.formats.thumbnail.url}
      ></img>
    );
  };

  return (
    <>
      <Head>
        <title>Ege Kıyı | {product[`name_${i18n.language}`]}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      {!isCategoryLoading ? (
        <div className="columns is-vcentered">
          <div style={{ alignSelf: "baseline" }} className="column is-5">
            <Slider {...settings}>
              {filteredProduct.map((i, index) => {
                return (
                  <div key={index}>
                    <figure className="image is-4by3">
                      <img
                        style={{ objectFit: "contain" }}
                        src={process.env.STRAPI_URL + i.image.url}
                        alt={i[`name_${i18n.language}`]}
                      />
                    </figure>
                  </div>
                );
              })}
            </Slider>
          </div>
          <div
            style={{ display: "flex", flexDirection: "column" }}
            className="column product-table"
          >
            <h1 className="title">{product[`name_${i18n.language}`]}</h1>
            <h3 className="subtitle "> {product.gross}</h3>
            <p className="is-size-5">
              {product[`description_${i18n.language}`]}
            </p>
            <br />
            <table className="table">
              <thead>
                <tr>
                  <th>{t("gross")}</th>
                  <th>{t("net")}</th>
                  <th>{t("quantity")}</th>
                  <th>{t("type")}</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{product.gross ? product.gross : "-"}</td>
                  <td>{product.net ? product.net : "-"} </td>
                  <td>{product.quantity}</td>
                  <td>{product[`type_${i18n.language}`]}</td>
                </tr>
              </tbody>
            </table>
            <h2 className="pl-5 subtitle has-text-left 	">{t("interested")}</h2>

            <div className=" pl-5 pr-5  product-suggestion-wrapper">
              {shuffledProduct.map((i, k) => (
                <Link
                  key={k}
                  href="/urun/[kategori]/[id]"
                  as={`/urun/${i[`name_${i18n.language}`]}/${i.id}`}
                >
                  <div key={k} className="card column product-item ">
                    <div className="card-image">
                      <figure className="image is-2by1">
                        <img
                          src={process.env.STRAPI_URL + i.image.url}
                          alt={i[`name_${i18n.language}`]}
                        />
                      </figure>
                    </div>
                    <div className="card-content">
                      <div className="media">
                        <div className="media-content">
                          <p className="title is-6">
                            {i[`name_${i18n.language}`]}
                          </p>
                          <p className="subtitle is-6">{i.gross}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </Link>
              ))}
            </div>
          </div>
        </div>
      ) : null}
    </>
  );
};

ProductDetail.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

ProductDetail.propTypes = {
  t: PropTypes.func.isRequired,
};

export default withTranslation("common")(ProductDetail);
