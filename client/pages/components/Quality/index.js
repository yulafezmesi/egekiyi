import React from "react";

const Quality = () => {
  return (
    <div className="quality">
      <section className="bd-focus hero is-medium has-bg-img hero-quality">
        <img src="assets/img/hero/food.png" className="hero-quality-bg"></img>
      </section>
    </div>
  );
};

export default Quality;
