import React from "react";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="footer-overlay"></div>
      <div className="has-text-centered  ">
        <div className="columns">
          <div className="column align-items-center-column">
            <img
              src="/assets/img/logo-beyaz.svg"
              alt="Ege Kıyı Logo"
              width="112"
              height="28"
            />
            <div>
              <p className="subtitle has-text-half-white is-size-6 adress">
                Ege Kıyı Gıda Ürt. Paz. Hayv. İnş. Nak. San. ve Tic. Ltd. Şti
                <span className="navbar-divider"></span>
                Yazıbaşı Mah. 329 Sok. No:4 Torbalı-İZMİR
                <span className="navbar-divider"></span>
                <a className="has-text-half-white" href="tel:+902328539585">
                  Tel:+90 232 853 95 85
                </a>
              </p>
            </div>
          </div>

          <div className="column ">
            <p className=" subtitle is-size-6 has-text-weight-semibold has-text-half-white">
              Kurumsal
            </p>
            <ul className="footer-menu">
              <li>
                <a href="/hakkimizda">Hakkımızda</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
