import React from "react";
import Header from "./Header";
import Footer from "./Footer";
import ContextProvider from "../../context";

function Layout({ children, isPrimary }) {
  return (
    <ContextProvider>
      <Header isPrimary={isPrimary} />
      {children}
      <Footer></Footer>
    </ContextProvider>
  );
}

export default Layout;
