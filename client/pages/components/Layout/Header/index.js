import React, { useContext, useState, useEffect } from "react";
import PropTypes from "prop-types";
import { i18n, withTranslation } from "../../../../i18n";
import { Context } from "../../../context";
import { Lang } from "../../Icons";
import Link from "next/link";
import { Instagram, Mail, Twitter } from "../../Icons/";
const Header = ({ t, isPrimary }) => {
  const { categories, isLoading } = useContext(Context);
  const [isOpen, setIsOpen] = useState(false);
  const [isHamburger, setIsHamburger] = useState(false);
  const [ofset, setOfset] = useState(0);
  useEffect(() => {
    window.onscroll = () => {
      scrollFunction();
    };
  }, []);

  const scrollFunction = () => {
    setOfset(document.documentElement.scrollTop);
    if (
      document.body.scrollTop > 80 ||
      document.documentElement.scrollTop > 80
    ) {
      document.getElementById("navbar").classList.add("navbar-sticky");
    } else {
      document.getElementById("navbar").classList.remove("navbar-sticky");
    }
  };

  return (
    <nav
      id="navbar"
      className={
        !isPrimary
          ? "bd-navbar navbar is-spaced is-fixed-top"
          : "bd-navbar navbar is-spaced is-fixed-top is-primary"
      }
    >
      <div className="navbar-brand">
        <a className="d-flex" href="/">
          <img
            className="logo is-hidden-desktop"
            style={{ cursor: "pointer" }}
            src="/assets/img/logo-beyaz.svg"
            alt="Ege Kıyı Logo"
            width="120"
            height="20"
          />
        </a>
        <div
          onClick={() => setIsHamburger(!isHamburger)}
          id="navbarBurger"
          className="navbar-burger burger"
          data-target="navMenuIndex"
        >
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>

      <div
        id="navMenuIndex"
        className={isHamburger ? "navbar-menu is-active" : "navbar-menu"}
      >
        <div className="navbar-align">
          <Link href="/">
            <a className="navbar-item bd-navbar-item-documentation hover-1 home">
              <span className="is-hidden-touch is-hidden-widescreen">
                {t("home")}
              </span>
              <span className="is-hidden-desktop-only">{t("home")}</span>
            </a>
          </Link>
          <a
            href="/galeri"
            className="navbar-item bd-navbar-item-documentation hover-1 catalog"
          >
            <span className="is-hidden-touch is-hidden-widescreen">
              {t("galler")}
            </span>
            <span className="is-hidden-desktop-only"> {t("gallery")}</span>
          </a>
          <div className="navbar-item has-dropdown is-hoverable">
            <a className="navbar-item w-100 bd-navbar-item-documentation hover-1">
              <span className="is-hidden-touch is-hidden-widescreen">
                {t("corporate")}
              </span>
              <span className="is-hidden-desktop-only">{t("corporate")}</span>
            </a>
            <div className="navbar-dropdown">
              <Link href="/hakkimizda">
                <a className="navbar-item ">{t("about")}</a>
              </Link>
            </div>
          </div>
          {ofset < 80 ? (
            <Link href="/">
              <div className="logo-wrapper">
                <a href="/">
                  <img
                    className="logo"
                    style={{ cursor: "pointer" }}
                    src="/assets/img/Logo.png"
                    alt="Ege Kıyı Logo"
                    width="190"
                    height="80"
                  />
                </a>
                <div className="contact">
                  <div className="adress">
                    <div className="is-text-white is-size-6">
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          justifyContent: "space-around",
                        }}
                      >
                        <Instagram onClick={() => { window.open('https://www.instagram.com/egekiyigida', ''); }} fill="white" width="35" />
                        <a href="mailto:info@egekiyi.com">
                          <Mail onClick={() => window.location.href = "mailto:info@egekiyi.com"} fill="white" width="35" />
                        </a>
                        <a>
                          <Twitter fill="white" width="35" />
                        </a>
                      </div>
                      <span style={{ textAlign: "center", display: "block" }}>
                        <a
                          className="has-text-half-white"
                          target="_blank"
                          href="tel:+902328539585"
                        >
                          Tel:+90 232 853 95 85
                        </a>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </Link>
          ) : (
            <div className="logo-wrapper">
              <a href="/">
                <img
                  className="logo"
                  style={{ cursor: "pointer" }}
                  src="/assets/img/logo-beyaz.svg"
                  alt="Ege Kıyı Logo"
                  width="190"
                  height="80"
                />
              </a>
            </div>
          )}

          <div
            onMouseEnter={() => setIsOpen(true)}
            className="navbar-item has-dropdown is-hoverable"
          >
            <Link
              shallow
              href="/urun/[id]"
              as={`/urun/${encodeURI(
                !isLoading
                  ? categories[0].urunlers[0][`name_${i18n.language}`]
                  : null
              )}`}
            >
              <a className="navbar-item bd-navbar-item-documentation hover-1">
                <span
                  style={{ textAlign: "left" }}
                  className="is-hidden-touch is-hidden-widescreen"
                >
                  {t("products")}
                </span>
                <span className="is-hidden-desktop-only">{t("products")}</span>
              </a>
            </Link>
            {isOpen ? (
              <>
                <div className="navbar-dropdown">
                  {categories.map((i, k) => (
                    <div key={k} className="nested navbar-item dropdown">
                      <div className="dropdown-trigger">
                        <a> {i[`name_${i18n.language}`]}</a>
                      </div>
                      <div
                        className="dropdown-menu"
                        id="dropdown-menu"
                        role="menu"
                      >
                        <div className="dropdown-content">
                          {[
                            ...new Map(
                              i.urunlers
                                .filter((i) => i.image)
                                .map((item) => [
                                  item[`name_${i18n.language}`],
                                  item,
                                ])
                            ).values(),
                          ].map((i, k) => (
                            <Link
                              shallow
                              href="/urun/[id]"
                              as={`/urun/${encodeURI(
                                i[`name_${i18n.language}`]
                              )}`}
                            >
                              <a
                                key={k}
                                href="#"
                                className="dropdown-item child-item"
                              >
                                {i[`name_${i18n.language}`]}
                              </a>
                            </Link>
                          ))}
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </>
            ) : null}
          </div>
          <a
            href="/e-katalog"
            className="navbar-item bd-navbar-item-documentation hover-1 catalog"
          >
            <span className="is-hidden-touch is-hidden-widescreen">
              {t("catalog")}
            </span>
            <span className="is-hidden-desktop-only"> {t("catalog")}</span>
          </a>
          <a
            className="navbar-item"
            onClick={() =>
              i18n.changeLanguage(i18n.language === "tr" ? "en" : "tr")
            }
          >
            <p>
              <Lang width="25" height="15"></Lang>{" "}
              {i18n.language === "tr" ? "English" : "Türkçe"}
            </p>
          </a>
        </div>
      </div>
    </nav>
  );
};

Header.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

Header.propTypes = {
  t: PropTypes.func.isRequired,
};

export default withTranslation("common")(Header);
