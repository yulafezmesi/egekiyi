import React, { useContext, useEffect, useState } from "react";
import Wrapper from "./wrapper";
import { Context } from "../../context";
import Card from "./card";

const Blog = () => {
  const { getBlogImages } = useContext(Context);
  const [images, setImages] = useState([]);
  useEffect(() => {
    const getPosts = async () => {
      let posts = await getBlogImages();
      setImages(posts);
    };
    getPosts();
  }, []);
  return (
    <Wrapper>
      {images.map((i, k) => (
        <>
          <Card i={i} key={k}></Card>
        </>
      ))}
    </Wrapper>
  );
};

export default Blog;
