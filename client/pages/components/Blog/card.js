import React, { useState } from "react";
import Lightbox from "react-image-lightbox";

const card = ({ i }) => {
  const [isShow, setIsShow] = useState(false);
  const [photoIndex, setPhotoIndex] = useState(0);
  return (
    <>
      <div onClick={() => setIsShow(true)} className="card gallery-card">
        <div className="card-image">
          <figure
            className="image 	
"
          >
            <img
              width="500"
              height="500"
              src={process.env.STRAPI_URL + i.images[0].url}
              alt={i.name}
              title={i.name}
            />
          </figure>
        </div>
        <div className="card-content">
          <div className="content">{i.title}</div>
        </div>
      </div>
      {isShow ? (
        <Lightbox
          mainSrc={process.env.STRAPI_URL + i.images[photoIndex].url}
          nextSrc={
            process.env.STRAPI_URL +
            i.images[(photoIndex + 1) % i.images.length].url
          }
          prevSrc={
            process.env.STRAPI_URL +
            i.images[(photoIndex + i.images.length - 1) % i.images.length].url
          }
          onCloseRequest={() => setIsShow(false)}
          onMovePrevRequest={() =>
            setPhotoIndex((photoIndex + i.images.length - 1) % i.images.length)
          }
          onMoveNextRequest={() =>
            setPhotoIndex((photoIndex + 1) % i.images.length)
          }
          imageCaption={i.images[photoIndex].caption}
        />
      ) : null}
    </>
  );
};

export default card;
