import React from "react";

const Wrapper = ({ children }) => {
  return <div className=" blog">{children}</div>;
};

export default Wrapper;
