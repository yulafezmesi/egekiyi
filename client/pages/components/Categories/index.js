import React from "react";
import PropTypes from "prop-types";
import Wrapper from "./wrapper";
import { withTranslation } from "../../../i18n";

const Index = ({ t }) => {
  return <Wrapper></Wrapper>;
};
Index.getInitialProps = async () => ({
  namespacesRequired: ["common", "home"],
});

Index.propTypes = {
  t: PropTypes.func.isRequired,
};

export default withTranslation("common")(Index);
