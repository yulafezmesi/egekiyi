import React, { useContext } from "react";
import Link from "next/link";
import { Context } from "../../context";
import { i18n } from "../../../i18n";
import { useRouter } from "next/router";

function LeftSide() {
  const router = useRouter();
  const { categories, getProductImagesByCategoryId, isLoading } = useContext(
    Context
  );
  return categories.map((i, k) => (
    <li key={k}>
      <a className="leftside-title" href="#">
        {i[`name_${i18n.language}`]}
      </a>
      <ul>
        {[
          ...new Map(
            i.urunlers
              .filter((i) => i.image)
              .map((item) => [item[`name_${i18n.language}`], item])
          ).values(),
        ].map((b, j) => (
          <li
            className={
              router.query.kategori === b[`name_${i18n.language}`]
                ? "link-active"
                : null
            }
            style={{ width: "180px", textAlign: "left" }}
            key={j}
          >
            <Link
              href="/urun/[id]"
              as={`/urun/${encodeURI(b[`name_${i18n.language}`])}`}
            >
              {b[`name_${i18n.language}`]}
            </Link>
          </li>
        ))}
      </ul>
    </li>
  ));
}

export default LeftSide;
