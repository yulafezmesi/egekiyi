import React from "react";
import PropTypes from "prop-types";
import { i18n, Link, withTranslation } from "../../../i18n";

const Categories = ({ children, t }) => {
  return (
    <section className="bd-focus">
      <nav className="pt-4 ml-6 mr-6 columns">{children}</nav>
      <img className="divider" src="/assets/img/divider.png"></img>
    </section>
  );
};

Categories.getInitialProps = async () => ({
  namespacesRequired: ["common", "home"],
});

Categories.propTypes = {
  t: PropTypes.func.isRequired,
};

export default withTranslation("common")(Categories);
