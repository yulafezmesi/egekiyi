import React, { useContext } from "react";
import Slider from "react-slick";
import { Context } from "../../context";
const Hero = () => {
  const { banners } = useContext(Context);
  var settings = {
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3500
  };
  return (
    <Slider {...settings}>
      {banners.map((i, k) => (
        <div key={k}>
          <div className="hero is-fullheight  has-background">
            <img
              alt="Ege Kıyı Acı Sos"
              className="hero-background is-transparent is-hidden-touch"
              src={process.env.STRAPI_URL + i.desktop.url}
            />
            <img
              alt="Ege Kıyı Acı Sos"
              className="hero-background is-transparent is-hidden-desktop"
              src={process.env.STRAPI_URL + i.touch.url}
            />
            <div className="hero-body">
              <div className="container"></div>
            </div>
          </div>
        </div>
      ))}
    </Slider>
  );
};

export default Hero;
