import React from "react";

const Categories = ({ children }) => {
  return (
    <section id="product-slider" >
      <img className="product-floor" src="/assets/img/product-floor.png"></img>
      <div >{children}</div>
    </section>
  );
};

export default Categories;
