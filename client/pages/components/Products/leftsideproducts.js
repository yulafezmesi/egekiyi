import React, { useContext, useEffect, useState } from "react";
import Link from "next/link";

import { Context } from "../../context";
import { i18n } from "../../../i18n";

const LeftSideProducts = ({ id }) => {
  const { getProductsByName } = useContext(Context);
  const [filteredProduct, setFilteredProduct] = useState([]);
  useEffect(() => {
    const getProducts = async () => {
      let product = await getProductsByName(`name_${i18n.language}=${id}`);
      setFilteredProduct(product);
    };
    getProducts();
  }, []);
  return (
    <>
      <div style={{ gap: "2rem" }} className="columns is-multiline ">
        {filteredProduct.map((i) => (
          <Link
            key={i.id}
            shallow
            href="/urun/[kategori]/[id]"
            as={`/urun/${encodeURI(i[`name_${i18n.language}`])}/${i.id}`}
          >
            <div
              style={{ minWidth: "150px" }}
              className="card column is-two-fifths product-item"
            >
              <div className="card-image ">
                <figure className="image  is-3by2">
                  <img
                    src={process.env.STRAPI_URL + i.image.url}
                    alt={i[`name_${i18n.language}`]}
                    title={i[`name_${i18n.language}`] + ` ${i.gross}`}
                  />
                </figure>
              </div>
              <div className="card-content">
                <div className="media">
                  <div className="media-content">
                    <p className="title is-4">{i[`name_${i18n.language}`]}</p>
                    <p className="subtitle is-6">{i.gross}</p>
                  </div>
                </div>
              </div>
            </div>
          </Link>
        ))}
      </div>
    </>
  );
};

export default LeftSideProducts;
