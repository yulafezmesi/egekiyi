import React, { useContext } from "react";
import Wrapper from "./wrapper";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { i18n } from "../../../i18n";
import Link from "next/link";
import Slider from "react-slick";

import { Context } from "../../context";

const index = () => {
  const { currentProducts } = useContext(Context);
  var settings = {
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    initialSlide: 3,
  };

  return (
    <Wrapper>
      <TransitionGroup>
        <Slider className="container" {...settings}>
          {currentProducts.map((i) => (
            <CSSTransition key={i.id} timeout={500} classNames="item">
              <a className="bd-focus-item column has-text-centered product-container">
                <figure className="product align-items-center">
                  {i.image ? (
                    <img
                      className="product-image"
                      src={`${process.env.STRAPI_URL}${i.image.formats.thumbnail.url}`}
                    ></img>
                  ) : null}
                </figure>
                <Link
                  href="/urun/[kategori]/[id]"
                  as={`/urun/${i[`name_${i18n.language}`]}/${i.id}`}
                >
                  <p
                    style={{ color: "var(--text-white)" }}
                    className="subtitle is-6 has-text-weight-semibold	"
                  >
                    {i[`name_${i18n.language}`]}
                    <span>{i.net ? i.net : i.gross}</span>
                  </p>
                </Link>
              </a>
            </CSSTransition>
          ))}
        </Slider>
      </TransitionGroup>
    </Wrapper>
  );
};

index.propTypes = {};

export default index;
