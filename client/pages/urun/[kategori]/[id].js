import React from "react";
import { useRouter } from "next/router";
import Layout from "../../components/Layout";
import ProductDetail from "../../components/ProductDetail";

const Post = () => {
  const router = useRouter();
  const { id } = router.query;
  return (
    <Layout isPrimary={true}>
     
      <section
        id="product-detail"
        className="hero is-default is-bold content-wrapper"
      >
        <div className="hero-body pt-0">
          <div className="container has-text-centered ">
            <ProductDetail id={id} />
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default Post;
