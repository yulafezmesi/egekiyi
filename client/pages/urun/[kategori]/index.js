import React from "react";
import { useRouter } from "next/router";
import Head from "next/head";
import Layout from "../../components/Layout";
import LeftSideCategories from "../../components/Categories/leftside";
import LeftSideProducts from "../../components/Products/leftsideproducts";

const Post = () => {
  const router = useRouter();
  const { kategori } = router.query;
  return (
    <Layout isPrimary={true}>
      <Head>
        <title>Ege Kıyı | {kategori}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <section className="hero is-default content-wrapper">
        <div className="hero-body pt-0">
          <div className="container has-text-centered ">
            <div className="flexwrap-reverse is-vcentered">
              <div className="column is-3">
                <section className="main-content columns is-fullheight is-size-6">
                  <aside className="is-2 is-narrow-mobile is-fullheight  ">
                    <ul className="menu-list">
                      <LeftSideCategories />
                    </ul>
                  </aside>
                </section>
              </div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                }}
                className="column"
              >
                <LeftSideProducts id={kategori} />
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default Post;
