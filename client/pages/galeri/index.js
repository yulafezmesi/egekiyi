import React from "react";
import Head from "next/head";
import Layout from "../components/Layout";
import Blog from "../components/Blog";
const Catalog = () => {
  return (
    <Layout isPrimary={true}>
      <Head>
        <title>Ege Kıyı | Galeri</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <section className="hero is-default content-wrapper">
        <div className="container">
          <Blog />
        </div>
      </section>
    </Layout>
  );
};

export default Catalog;
