import React, { createRef } from "react";
import Head from "next/head";
import Layout from "../components/Layout";
import HTMLFlipBook from "react-pageflip";
import Slider from "react-slick";
const Catalog = () => {
  var settings = {
    infinite: false,
    speed: 500,
    dots: true,
    autoPlay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
  };
  const flip = createRef();
  return (
    <Layout isPrimary={true}>
      <Head>
        <title>Ege Kıyı | E-Katalog</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <section className="hero is-default content-wrapper">
        <div className="is-hidden-mobile" style={{ position: "relative" }}>
          <HTMLFlipBook
            width={500}
            height={733}
            size="stretch"
            minWidth={200}
            maxWidth={500}
            usePortrait
            minHeight={400}
            maxHeight={1533}
            maxShadowOpacity={0.5}
            mobileScrollSupport={true}
            className="flip-wrapper"
            ref={flip}
          >
            <div></div>
            <img src="./assets/img/katalog/kapak.jpg" className="page-image" />
            <img src="./assets/img/katalog/1.jpg" className="page-image" />
            <img src="./assets/img/katalog/2.jpg" className="page-image" />
            <img src="./assets/img/katalog/3.jpg" className="page-image" />
            <img src="./assets/img/katalog/4.jpg" className="page-image" />
            <img src="./assets/img/katalog/5.jpg" className="page-image" />
            <img src="./assets/img/katalog/6.jpg" className="page-image" />
            <img src="./assets/img/katalog/7.jpg" className="page-image" />
            <img src="./assets/img/katalog/8.jpg" className="page-image" />
            <img src="./assets/img/katalog/9.jpg" className="page-image" />
            <img src="./assets/img/katalog/10.jpg" className="page-image" />
            <img
              src="./assets/img/katalog/arka_kapak.jpg"
              className="page-image"
            />
          </HTMLFlipBook>
          <button
            type="button"
            data-role="none"
            className="slick-arrow slick-next flip-next"
            onClick={() => flip.current.getPageFlip().flipNext()}
          >
            Next
          </button>
          <button
            onClick={() => flip.current.getPageFlip().flipPrev()}
            type="button"
            data-role="none"
            className="slick-arrow slick-prev flip-prev"
          >
            Prev
          </button>
        </div>
        <div className="is-hidden-desktop">
          <Slider {...settings}>
            <img src="./assets/img/katalog/kapak.jpg" className="page-image" />
            <img src="./assets/img/katalog/1.jpg" className="page-image" />
            <img src="./assets/img/katalog/2.jpg" className="page-image" />
            <img src="./assets/img/katalog/3.jpg" className="page-image" />
            <img src="./assets/img/katalog/4.jpg" className="page-image" />
            <img src="./assets/img/katalog/5.jpg" className="page-image" />
            <img src="./assets/img/katalog/6.jpg" className="page-image" />
            <img src="./assets/img/katalog/7.jpg" className="page-image" />
            <img src="./assets/img/katalog/8.jpg" className="page-image" />
            <img src="./assets/img/katalog/9.jpg" className="page-image" />
            <img src="./assets/img/katalog/10.jpg" className="page-image" />
            <img
              src="./assets/img/katalog/arka_kapak.jpg"
              className="page-image"
            />
          </Slider>
        </div>
      </section>
    </Layout>
  );
};

export default Catalog;
