import Head from "next/head";
import Hero from "./components/Hero";
import Layout from "./components/Layout/";
import Categories from "./components/Categories/";
import Products from "./components/Products/";
export default function Home() {
  return (
    <>
      <Head>
        <title>Ege Kıyı</title>
        <link rel="icon" href="/favicon.ico" />
        <meta property="og:url" content="https://egekiyi.com" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Ege Kıyı" />
        <meta
          property="og:description"
          content="Ege Kıyı Lezzetleri"
        />
        <meta
          property="og:image"
          content="https://www.egekiyi.com/assets/img/organic.png"
        />

        <meta name="twitter:card" content="summary" />
        <meta property="twitter:domain" content="egekiyi.com" />
        <meta property="twitter:url" content="https://egekiyi.com" />
        <meta name="twitter:title" content="Ege Kıyı" />
        <meta
          name="twitter:description"
          content="Ege Kıyı Lezzetleri"
        />
        <meta
          name="twitter:image"
          content="https://www.egekiyi.com/assets/img/organic.png"
        ></meta>
      </Head>
      <Layout>
        <main>
          <Hero />
          <section className="category-section">
            <Products />
          </section>
        </main>
      </Layout>
    </>
  );
}
