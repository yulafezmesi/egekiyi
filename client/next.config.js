const path = require("path");
const { nextI18NextRewrites } = require("next-i18next/rewrites");
const localeSubpaths = {};
module.exports = {
  env: {
    STRAPI_URL: process.env.STRAPI_URL,
  },
  rewrites: async () => nextI18NextRewrites(localeSubpaths),
  publicRuntimeConfig: {
    localeSubpaths,
  },
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
};
